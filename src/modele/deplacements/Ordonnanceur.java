package modele.deplacements;

import modele.plateau.EntiteDynamique;
import modele.plateau.Jeu;

import java.util.ArrayList;
import java.util.Observable;

import static java.lang.Thread.*;

public class Ordonnanceur extends Observable implements Runnable {
    private Jeu jeu;
    private ArrayList<RealisateurDeDeplacement> lstDeplacements = new ArrayList<RealisateurDeDeplacement>();
    private long pause;

    public void add(RealisateurDeDeplacement deplacement) {
        lstDeplacements.add(deplacement);
    }

    public Ordonnanceur(Jeu _jeu) {
        jeu = _jeu;
    }

    public void start(long _pause) {
        pause = _pause;
        new Thread(this).start();
    }

    @Override
    public void run() {
        boolean update = false;

        while(true) {
            jeu.resetCmptDepl();
      
            if(this.jeu.getNbVies() == 0){
                this.jeu.modifierScore(-150);
                this.jeu.sauvergarderEtResetScore();
                resetMap(1); 
                resetMap(1); // pour evite certain bug d'affichage
            }
            
            //si le héros meurt, on recharge le niveau
            if(!this.jeu.getHector().estVivant()){
                this.jeu.modifierScore(-150);
                resetMap(this.jeu.getNumeroMap());
                resetMap(this.jeu.getNumeroMap()); // pour evite certain bug d'affichage
                update = true;
            }
            
            //Si le niveau a été complété, on passe à la map suivante
            if(this.jeu.getNbBombes() == 0){
                System.out.println("TOUTES LES BOMBES ONT ETE RAMASSEES");
                resetMap(this.jeu.getNumeroMap() + 1);
                resetMap(this.jeu.getNumeroMap()); // pour evite certain bug d'affichage
                update = true;
            }

            for(int i = 0; i<lstDeplacements.size(); i++){
                if(!this.jeu.getHector().estVivant() || this.jeu.getNbVies() == 0 || this.jeu.getNbBombes() == 0){
                    i = lstDeplacements.size(); 
                }
                else{
                    RealisateurDeDeplacement d = lstDeplacements.get(i);
                    if (d.realiserDeplacement()){  
                        update = true;
                    }
                }
            }

            Controle4Directions.getInstance().resetDirection();
            
            
            if (update) {
                setChanged();
                notifyObservers();
            }           

            try {
                sleep(pause);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void resetMap(int nbMap){
        for (RealisateurDeDeplacement d : lstDeplacements) {
            d.reset();
        }
        lstDeplacements.clear();
        this.jeu.getHector().setVivant(true);
        this.jeu.reset(nbMap);
    }

    public boolean removeEntiteDynamique(EntiteDynamique e){
        boolean ret = false;
        for(int i = 0; i<lstDeplacements.size(); i++){
            if(e instanceof modele.plateau.Heros){
                resetMap(this.jeu.getNumeroMap());
            }
            else if(e instanceof modele.plateau.Bot){
                RealisateurDeDeplacement d = lstDeplacements.get(i);
                if(d instanceof IA && d.getListeEntitesDynamiques().indexOf(e) != -1){
                    d.removeRealisateurDeDeplacement(e);
                    lstDeplacements.remove(d);
                }
                if(d instanceof Gravite && d.getListeEntitesDynamiques().indexOf(e) != -1 ){
                    d.removeRealisateurDeDeplacement(e);
                }
                ret = true;
            }
        }
        return ret;
    }

    public ArrayList<RealisateurDeDeplacement> getLstDeplacements(){
        return lstDeplacements;
    }
}
