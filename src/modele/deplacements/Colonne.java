package modele.deplacements;
import modele.plateau.EntiteDynamique;

/**
 * A la reception d'une commande, toutes les cases (EntitesDynamiques) des colonnes se déplacent dans la direction définie
 * (vérifier "collisions" avec le héros)
 */
public class Colonne extends RealisateurDeDeplacement {
    private Direction directionCourante;
    private static Colonne listColonnesRouge;
    private static Colonne listColonnesBleue;
    private String couleur;

    /*public Colonne(String couleur) {
        this.couleur = couleur;
        
    }*/

    public static Colonne getInstancesRouge(){
        if(listColonnesRouge == null){
            listColonnesRouge = new Colonne();
        }
        return listColonnesRouge;
    }

    public static Colonne getInstancesBleue(){
        if(listColonnesBleue == null){
            listColonnesBleue = new Colonne();
        }
        return listColonnesBleue;
    }

    public void setDirectionCourante(Direction _directionCourante) {
        directionCourante = _directionCourante;
    }

    protected boolean realiserDeplacement() { 
        boolean ret = false;

        for (int i = 0; i < lstEntitesDynamiques.size(); i++) {
            EntiteDynamique e = lstEntitesDynamiques.get(i);
            if (directionCourante != null){
                if(e.avancerDirectionChoisie(directionCourante)) { ret = true; }
            }
        }
        return ret;
    }

    public String getCouleur() {
        return this.couleur;
    }

    public void resetDirection() {
        directionCourante = null;
    }
}
