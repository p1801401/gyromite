package modele.plateau;

public class Echelle extends EntiteStatique {
    public Echelle(Jeu _jeu) { super(_jeu); }

    public boolean peutEtreEcrase() { return false; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return true; }
    public boolean peutEtreTraversee() { return true; }
    public boolean peutEtreTuee() { return false; }
    public boolean peutEtreRamassee() { return false; }
}
