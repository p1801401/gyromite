/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

/**
 * Héros du jeu
 */
public class Heros extends EntiteDynamique {
    private boolean vivant;
    public Heros(Jeu _jeu) {
        super(_jeu);
        this.vivant = true;
    }

    public boolean estVivant(){
        return vivant;
    }

    public void setVivant(boolean vivant){
        this.vivant = vivant; 
    }

    public boolean peutEtreEcrase() { return true; }
    public boolean peutServirDeSupport() { return false; }
    public boolean peutPermettreDeMonterDescendre() { return false; }
    public boolean peutEtreTraversee(){ return true; }
    public boolean peutEtreTuee() { return true; }
    public boolean peutEtreRamassee() { return false; }
}
