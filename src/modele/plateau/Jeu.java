/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import modele.deplacements.Colonne;
import modele.deplacements.Controle4Directions;
import modele.deplacements.Direction;
import modele.deplacements.Gravite;
import modele.deplacements.Ordonnanceur;
import modele.deplacements.IA;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Actuellement, cette classe gère les postions (ajouter conditions de victoire,
 * chargement du plateau, etc.)
 */
public class Jeu {

    public static final int SIZE_X = 30;
    public static final int SIZE_Y = 15;
    public static final int VIES_MAX = 3;
    public static int MEILLEUR_SCORE;

    // compteur de déplacements horizontal et vertical (1 max par défaut, à chaque
    // pas de temps)
    private HashMap<Entite, Integer> cmptDeplH = new HashMap<Entite, Integer>();
    private HashMap<Entite, Integer> cmptDeplV = new HashMap<Entite, Integer>();

    private Heros hector;

    private HashMap<Entite, Point> map = new HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à
                                                                       // partir de sa référence
    private ArrayList<Entite>[][] grilleEntites = new ArrayList[Jeu.SIZE_X][Jeu.SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées

    private Ordonnanceur ordonnanceur = new Ordonnanceur(this);

    private int numeroMap;
    private int nbVies;
    private int nbBombes;
    private int score; 

    public Jeu() {
        for(int x = 0; x < Jeu.SIZE_X; x++){
            for(int y = 0; y < Jeu.SIZE_Y; y++){
                this.grilleEntites[x][y] = new ArrayList<>();
            }
        }
        this.nbVies = Jeu.VIES_MAX;
        this.score = 0;
        initialisationDesEntites(1);
    }

    public void reset(int nbMap){


        for(int x = 0; x < Jeu.SIZE_X; x++){
            for(int y = 0; y < Jeu.SIZE_Y; y++){
                for(int z = 0; z < this.grilleEntites[x][y].size(); z++){
                    this.grilleEntites[x][y].remove(z);
                }
            }
        }
        map.clear();
        
        File fileMaps = new File("./assets/maps/");
        int nbMaps = fileMaps.listFiles().length;
        //on a atteint le dernier file dans le dossier map, on lance la map 1  
        if(nbMap > nbMaps){ 
            sauvergarderEtResetScore();
            initialisationDesEntites(1); 
        } 
        else { initialisationDesEntites(nbMap); }
    }

    public void resetCmptDepl() {
        cmptDeplH.clear();
        cmptDeplV.clear();
    }

    public void start(long _pause) {
        ordonnanceur.start(_pause);
    }

    public ArrayList<Entite>[][] getGrille() {
        return grilleEntites;
    }

    public Heros getHector() {
        return hector;
    }

    public int getNbBombes(){
        return this.nbBombes;
    }

    public int getNumeroMap(){
        return numeroMap;
    }

    public int getNbVies(){
        return this.nbVies;
    }

    public int getScore() {
        return this.score;
    }

    public void resetScore() {
        this.score = 0;
    }

    public void sauvergarderEtResetScore() {
        if (this.score > Jeu.MEILLEUR_SCORE) {
            Jeu.MEILLEUR_SCORE = this.score;
            sauvegarderScore();
            System.out.println("score reset!");
        }
        resetScore();
    }
    public void modifierScore(int diffS) {
        this.score += diffS;
    }

    public void sauvegarderScore() {
        File scoreFile = new File("./assets/best_score.txt");
        try {
            FileWriter fileWriter = new FileWriter(scoreFile);
            BufferedWriter buffWriter = new BufferedWriter(fileWriter);
            buffWriter.write(Integer.toString(Jeu.MEILLEUR_SCORE));
            buffWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }

    public int recupererScore() {
        int s = -1;
        File scoreFile = new File("./assets/best_score.txt");
        try {
            FileReader fileReader = new FileReader(scoreFile);
            BufferedReader buffReader = new BufferedReader(fileReader);
            String buffer = buffReader.readLine();
            s = Integer.parseInt(buffer);
            System.out.println("s : " + s);
            buffReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return s;
    }


    private void initialisationDesEntites(int nbMap) {
        File mapFile = new File("./assets/maps/map" + Integer.toString(nbMap) + ".txt");
        //Initialisation des données
        int obj = 0;
        int x = 0;
        int y = 0;
        this.nbBombes = 0;

        Jeu.MEILLEUR_SCORE = recupererScore();
        System.out.println("meilleur score : " + Jeu.MEILLEUR_SCORE);

        //Update numéro map
        this.numeroMap = nbMap;
        //Update nombre vies si nécessaire
        if (this.nbVies == 0) this.nbVies = Jeu.VIES_MAX;

        try {
            FileReader fileReader = new FileReader(mapFile);
            BufferedReader buffReader = new BufferedReader(fileReader);
            //System.out.println(mapFile.getCanonicalPath());
            Gravite g = new Gravite();
            //On lit caractère par caractère
            while ((obj = buffReader.read()) != -1) {
                //Convertir l'entier en char
                char ch = (char) obj;
                switch (ch) {
                case 'M':
                    addEntite(new Mur(this), x, y);
                    x++;
                    break;
                case 'E':
                    addEntite(new Echelle(this), x, y);
                    x++;
                    break;
                case 'H':
                    hector = new Heros(this);
                    addEntite(hector, x, y);
                    g.addEntiteDynamique(hector);
                    ordonnanceur.add(g);
                    Controle4Directions.getInstance().addEntiteDynamique(hector);
                    ordonnanceur.add(Controle4Directions.getInstance());
                    hector.setVivant(true);
                    x++;
                    break;
                case 'R':
                    modele.plateau.Colonne colonneR = new modele.plateau.Colonne(this,"rouge");
                    addEntite(colonneR, x, y);
                    Colonne.getInstancesRouge().addEntiteDynamique(colonneR);
                    ordonnanceur.add(Colonne.getInstancesRouge());
                    x++;
                    break;
                case 'B':
                    modele.plateau.Colonne colonneB = new modele.plateau.Colonne(this,"bleue");
                    addEntite(colonneB, x, y);
                    Colonne.getInstancesBleue().addEntiteDynamique(colonneB);
                    ordonnanceur.add(Colonne.getInstancesBleue());
                    x++;
                    break; 
                case 'S':
                    Bot bot = new Bot(this);
                    addEntite(bot, x, y);
                    IA botIA = new IA();
                    g.addEntiteDynamique(bot);
                    botIA.addEntiteDynamique(bot);
                    ordonnanceur.add(botIA);
                    x++;
                    break;
                case 'D':
                    Bombe dynamite = new Bombe(this);
                    addEntite(dynamite, x, y);
                    this.nbBombes++;
                    x++;
                    break;
                case ' ':
                    x++;
                    break;
                default : 
                    break;
                }

                if (x >= Jeu.SIZE_X) {
                    y++;
                    x = 0;
                }
            }

            //On réinitialise la direction à null pour les colonnes
            Colonne.getInstancesRouge().setDirectionCourante(null);
            Colonne.getInstancesBleue().setDirectionCourante(null);

            //Affichage console du status du jeu en cours
            System.out.println("-----------GAME STATUS----------");
            System.out.println("Numéro de la map : " + nbMap);
            System.out.println("Nombre de vies restantes : " + this.nbVies);
            System.out.println("Nombre totale de bombes : " + this.nbBombes);
            System.out.println("--------------------------------");

            buffReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Fichier introuvable");
            System.err.println(e);
        } catch (IOException ioe) {
            System.out.println("Exception " + ioe);
        }

    }


    private void addEntite(Entite e, int x, int y) {
        grilleEntites[x][y].add(e);
        map.put(e, new Point(x, y));
    }

    /**
     * Permet par exemple a une entité de percevoir sont environnement proche et de
     * définir sa stratégie de déplacement
     *
     */
    public Entite regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }

    /**
     * Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il
     * est réalisé Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        boolean retour = false;
        Point pCourant = map.get(e);
        
        Point pCible = calculerPointCible(pCourant, d);

        if (contenuDansGrille(pCible) && (objetALaPosition(pCible) == null || gestionCollision(pCourant, pCible, d))) { // a adapter (collisions murs, etc.)
            // compter le déplacement : 1 deplacement horizontal et vertical max par pas de
            // temps par entité
            switch (d) {
            case bas:
            case haut:
                if (cmptDeplV.get(e) == null) {
                    cmptDeplV.put(e, 1);

                    retour = true;
                }
                break;
            case gauche:
            case droite:
                if (cmptDeplH.get(e) == null) {
                    cmptDeplH.put(e, 1);
                    retour = true;

                }
                break;
            }
        }

        if (retour) {
            deplacerEntite(pCourant, pCible, e);
        }

        return retour;
    }

    public Boolean gestionCollision(Point pCourant, Point pCible, Direction d){
        Entite eCourante = objetALaPosition(pCourant);
        Entite eCible = objetALaPosition(pCible);
        if(eCible != null){
            if(eCourante instanceof modele.plateau.Colonne) {
                // Si un héros ou un bot se trouve sur une colonne, si la colonne bouge, ce dernier bouge aussi 

                //Ecrasement du héros ou un bot
                if(eCible.peutEtreEcrase() && eCible.regarderDansLaDirection(d) != null && !eCible.regarderDansLaDirection(d).peutEtreTraversee()){ 
                    if(eCible instanceof Heros){
                        this.hector.setVivant(false);
                        --this.nbVies; 
                    }
                    else{
                        this.ordonnanceur.removeEntiteDynamique((EntiteDynamique)eCible); 
                        grilleEntites[pCible.x][pCible.y].remove(eCible);
                        map.remove(eCible);
                    }
                    return true;          
                }    
                //le héros ou un bot monte ou descend avec la colonne
                else if(eCible.peutEtreEcrase() && eCible.regarderDansLaDirection(d) == null){              
                    deplacerEntite(eCible, d);
                    return true;
                }
                return false;  
            }
            else if(eCible.peutPermettreDeMonterDescendre()){
                return true;
            }
            else if(eCible.peutEtreTuee()){
                this.hector.setVivant(false);
                grilleEntites[pCible.x][pCible.y].remove(eCible);
                map.remove(eCible);               
                --this.nbVies; 
                
                return true; 
            }
            else if(!eCible.peutEtreTraversee()){
                return false;    
            }
            if (eCourante instanceof Heros) {
                if(objetALaPosition(pCible).peutEtreRamassee()) {
                    Bombe b = (Bombe) objetALaPosition(pCible); //On sait que c'est une bombe
                    this.grilleEntites[pCible.x][pCible.y].remove(b);
                    this.map.remove(b);
                    --this.nbBombes;
                    this.score += 50;
                    if (this.nbBombes == 0) this.score += 100;
                    System.out.println("score : " + this.score);
                    return true;
                }
            }
        }
        return true;
    }

    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;

        switch (d) {
        case haut:
            pCible = new Point(pCourant.x, pCourant.y - 1);
            break;
        case bas:
            pCible = new Point(pCourant.x, pCourant.y + 1);
            break;
        case gauche:
            pCible = new Point(pCourant.x - 1, pCourant.y);
            break;
        case droite:
            pCible = new Point(pCourant.x + 1, pCourant.y);
            break;

        }

        return pCible;
    }

    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        grilleEntites[pCourant.x][pCourant.y].remove(e);
        grilleEntites[pCible.x][pCible.y].add(e);
        map.put(e, pCible);
    }

    /**
     * Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }

    private Entite objetALaPosition(Point p) {
        Entite retour = null;

        if (contenuDansGrille(p) && grilleEntites[p.x][p.y].size() != 0) {
            // de base y avait: retour = grilleEntites[p.x][p.y];
            retour = grilleEntites[p.x][p.y].get(0) == null ? null : grilleEntites[p.x][p.y].get(0) ;
        }
        return retour;
    }

    public Ordonnanceur getOrdonnanceur() {
        return ordonnanceur;
    }

}
