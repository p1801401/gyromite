package modele.plateau;

public class Colonne extends EntiteDynamique {
    private String couleur;

    public Colonne(Jeu _jeu, String couleur) { 
        super(_jeu);
        this.couleur = couleur;
    }

    public String getCouleur() {
        return this.couleur;
    }

    public boolean peutEtreEcrase() { return false; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return false; }
    public boolean peutEtreTraversee(){ return false; }
    public boolean peutEtreTuee() { return false; }
    public boolean peutEtreRamassee() { return false; }
}
