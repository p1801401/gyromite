package VueControleur;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

import modele.deplacements.Controle4Directions;
import modele.deplacements.Direction;
import modele.plateau.*;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleurGyromite extends JFrame implements Observer {
    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    // icones affichées dans la grille
    private ImageIcon icoHero;
    private ImageIcon icoVide;
    private ImageIcon icoMur;
    private ImageIcon icoColonneRouge;
    private ImageIcon icoColonneBleue;
    private ImageIcon icoBot;
    private ImageIcon icoEchelle;
    private ImageIcon icoBombe;

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)
    private JLabel[][] tabJLabelInfos; 
    private JLabel score; 
    private JLabel nbVies;   
    private JLabel nbMap;   
    private JLabel highestScore;  
    private JLabel background; 

    JFrame accueil; 

    public VueControleurGyromite(Jeu _jeu) {
        sizeX = Jeu.SIZE_X;
        sizeY = Jeu.SIZE_Y;
        jeu = _jeu;

        chargerLesIcones();
        placerLesComposantsGraphiques();
        ajouterEcouteurClavier();
        mettreAJourAffichage();

    }

    

    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT : Controle4Directions.getInstance().setDirectionCourante(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : Controle4Directions.getInstance().setDirectionCourante(Direction.droite); break;
                    case KeyEvent.VK_DOWN : Controle4Directions.getInstance().setDirectionCourante(Direction.bas); break;
                    case KeyEvent.VK_UP : Controle4Directions.getInstance().setDirectionCourante(Direction.haut); break;
                    case KeyEvent.VK_A : modele.deplacements.Colonne.getInstancesRouge().setDirectionCourante(Direction.haut); break;
                    case KeyEvent.VK_Q : modele.deplacements.Colonne.getInstancesRouge().setDirectionCourante(Direction.bas); break;
                    case KeyEvent.VK_Z : modele.deplacements.Colonne.getInstancesBleue().setDirectionCourante(Direction.haut); break;
                    case KeyEvent.VK_S : modele.deplacements.Colonne.getInstancesBleue().setDirectionCourante(Direction.bas); break;
                }
            }
        });
    }


    private void chargerLesIcones() {
        icoHero = chargerIcone("./assets/Images/Pacman.png");
        icoVide = chargerIcone("./assets/Images/Vide.png");
        icoColonneRouge = chargerIcone("./assets/Images/ColonneRouge.png");
        icoColonneBleue = chargerIcone("./assets/Images/ColonneBleue.png");
        icoMur = chargerIcone("./assets/Images/Mur.png");
        icoBot = chargerIcone("./assets/Images/Fantome.png");
        icoEchelle = chargerIcone("./assets/Images/Echelle.png");
        icoBombe = chargerIcone("./assets/Images/Bombe.png");
    }

    private ImageIcon chargerIcone(String urlIcone) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurGyromite.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex);
            return null;
        }

        return new ImageIcon(image);
    }

    private void placerLesComposantsGraphiques() {
        
        setTitle("Gyromite");
        setSize(680, 480);
        this.setLocationRelativeTo(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre
        
        background = new JLabel(new ImageIcon("./assets/Images/background.png"));

        add(background);
        background.setLayout(new FlowLayout());

        JComponent grilleInfos = new JPanel(new GridLayout(1, 4));
        grilleInfos.setBounds(0, 0, 580, 20);
        tabJLabelInfos = new JLabel[3][1];
        score = new JLabel("Score : " + Integer.toString(0), SwingConstants.CENTER);
        highestScore = new JLabel("Highest Score : " + Integer.toString(0), SwingConstants.CENTER);
        nbMap = new JLabel("Map n°" + Integer.toString(1), SwingConstants.CENTER);
        nbVies = new JLabel("Vies" + Integer.toString(3), SwingConstants.CENTER);

        grilleInfos.add(score);
        grilleInfos.add(highestScore);
        grilleInfos.add(nbMap);
        grilleInfos.add(nbVies);

        background.add(grilleInfos);
        
        JComponent grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille        
        tabJLabel = new JLabel[sizeX][sizeY];

        

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                JLabel jlab = new JLabel();
                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        background.add(grilleJLabels);

        


    }
    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if(jeu.getGrille()[x][y].size() != 0){
                    for(int z = 0; z < jeu.getGrille()[x][y].size(); z++){
                        if (jeu.getGrille()[x][y].get(z) instanceof Heros) { // si la grille du modèle contient un Pacman, on associe l'icône Pacman du côté de la vue
                            tabJLabel[x][y].setIcon(icoHero);
                        } 
                        else if (jeu.getGrille()[x][y].get(z) instanceof Mur) {
                            tabJLabel[x][y].setIcon(icoMur);
                        } else if (jeu.getGrille()[x][y].get(z) instanceof Colonne) {
                            if (((Colonne) jeu.getGrille()[x][y].get(z)).getCouleur() == "rouge") {
                                tabJLabel[x][y].setIcon(icoColonneRouge);
                            } else {
                                tabJLabel[x][y].setIcon(icoColonneBleue);
                            }
                            
                        } else if (jeu.getGrille()[x][y].get(z) instanceof Bot) {
                            tabJLabel[x][y].setIcon(icoBot);
                        } 
                        else if (jeu.getGrille()[x][y].get(z) instanceof Echelle) {
                            tabJLabel[x][y].setIcon(icoEchelle);
                        }
                        else if (jeu.getGrille()[x][y].get(z) instanceof Bombe) {
                            tabJLabel[x][y].setIcon(icoBombe);
                        }

                    }
                } else { tabJLabel[x][y].setIcon(icoVide); }
            }
        }

        score.setText("Score : " + Integer.toString(this.jeu.getScore()));
        highestScore.setText("Highest Score : " + Integer.toString(Jeu.MEILLEUR_SCORE));
        nbMap.setText("Map n°" + Integer.toString(this.jeu.getNumeroMap()));
        nbVies.setText("Vies : " + Integer.toString(this.jeu.getNbVies()));
    }

    @Override
    public void update(Observable o, Object arg) {

        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                }); 
    }
}
